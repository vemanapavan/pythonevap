__author__ = 'pavanteja'


def wrapper(func):
    def check(a, b):
        if 0 < a < b < 4:
            ret = func(a, b)
        else:
            a -= 2
            b -= 2
            ret = func(a, b)

        return ret
    return check


@wrapper
def add(x, y):
    return x + y


print(add(3,9))