def except_test1():
    try:
        import termios, TERMIOS
    except ImportError:
        try:
            import msvcrt
        except ImportError:
            try:
                from EasyDialogs import AskPassword
            except ImportError:
                getpass = 'x'
            else:
                getpass = 'y'
        else:
            getpass = 'z'
    else:
        getpass = 'strong'
    print(getpass)

custom_except_test()
