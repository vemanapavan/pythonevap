
# coding: utf-8

# In[ ]:

#In python3.4 range is written as xrange which saves memory

for i in range(3,9):
    print(i,end=',')


# In[ ]:

#for loop variable global namespace leaking solved in python3.4

i = 0
li = [i for i in range(2,8)]
print(li)
print(i) #In python2 i will be 7


# In[ ]:

'''
break continue and else clauses
else clause is for loop which runs when no break occurs in the loop
'''

for i in range(2,100):
    for j in range(2,i):
        if i % j == 0:
            break
    else:
        print(i,'is a prime number')


# In[10]:

def buildConnectionString(params):
    '''
    Build a connection string from a dictionary of parameters
    '''
    
    return ';'.join(["%s=%s" % (k,v) for k,v in params.items()])


if __name__ == '__main__':
    params = {'server':'tomcat','database':'user','uid':'293d','passwd':'hello'}
    print(buildConnectionString(params)) 


# In[ ]:

#Dictionary

dic = {'name':'Pavan','Designation':'Jr.Software Engineer','EmpId':23323}

print(dic['name'],dic['EmpId'])

print(dic.items())

print(dic.keys(),dic.values())

for key,val in dic.items():
    print(key,':',val)

del dic['EmpId']

print(dic)

dic.clear()

print(dic)


# In[ ]:

#Lists

li = ['a','b',1,2,3.4,True]

print(li[5])

if li[5]:
    print('Correct!')

print(li[-3]) # 3 element from the end

print(li[:3]) #Slicing list

#lists are mutable

li.append("new") 
li.extend(['One','Two']) #expects a list as argument

print(li)

#Searching lists
print(li.index('One')) #case sensitive
'new' in li

#Deleting elements in list
li.remove('a')
li.pop()

print(li)


# In[ ]:

# Tuple - Immutable list
tp = (1,2,'Hey',3.4)

#tp[1] = 3 #Throws error

(a,b) = range(0,2)


# In[ ]:

import math
def info(object,spacing=10,collapse=1):
    '''
    Printing methods and doc strings
    '''
    methods = [method for method in dir(object) if callable(getattr(object,method))] #returns list of callable methods
    
    processing = collapse and (lambda s: " ".join(s.split())) or (lambda s:s) #processes multi line doc strings
    
    print('\n'.join(["%s %s" %( method.ljust(spacing),  processing(str(getattr(object,method).__doc__))) for method in methods]))

info(math)
    


# In[51]:




# In[ ]:



