def except_test():
    '''
    Using inbuilt exceptions
    '''
    try:
        fp = open('test')
    except IOError:
        print("The file doesn't exist")
    else:
        print('Opened file %s in %s mode' %(fp.name,fp.mode))
        print('Here is the content: ' ,fp.readlines())
        print('Cursor is at %d th byte' %fp.tell())
        print('Seeking....',fp.seek(4,0)) #When opened in a non-binary mode only seeks relative to the beginning works
        print('Cursor position after moving : %d th byte' %fp.tell() )

except_test()	
